//
//  MediaSubs.swift
//  MediaSubs
//
//  Created by Jonnie on 02/03/2020.
//

import Foundation
import SwiftyJSON


public final class MediaSubs {

    let name = "MediaSubs"
    
    public init() {}
    
    public func addSubtitlesToVideo(cloudName: String, videoPublicId: String, subtitles: JSON) -> String {
        let subtitlesArr = subtitles["Subtitles"].array
        if (subtitlesArr!.count == 0){
            return ""
        }
        
        var captions: [String] = []
        for subtitle in subtitlesArr! {
            let startTiming = subtitle["start-timing"].doubleValue
            let endTiming = subtitle["end-timing"].doubleValue
            let text = subtitle["text"].stringValue.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: ",").inverted)!
            let caption = String(format: "l_text:arial_28:%@,g_south,y_80,so_%.3f,eo_%.3f,co_rgb:ffffff",
                text,
                startTiming,
                endTiming
            )
            captions.append(caption.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)
        }

        let url = String(format: "%@/%@/video/upload/%@/%@",
            "https://res.cloudinary.com",
            cloudName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!,
            captions.joined(separator: "/"),
            videoPublicId.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        )
        return url
    }

}
