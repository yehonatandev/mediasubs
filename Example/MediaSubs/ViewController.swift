//
//  ViewController.swift
//  MediaSubs
//
//  Created by yehonatandev on 03/02/2020.
//  Copyright (c) 2020 yehonatandev. All rights reserved.
//

import UIKit
import MediaSubs
import AVKit
import AVFoundation
import SwiftyJSON

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var subsView: UIView!
    @IBOutlet weak var cloudName: UITextField!
    @IBOutlet weak var videoPublicId: UITextField!
    
    let scrollView = UIScrollView()
    
    let stackView = UIStackView()
    
    var textFields: [[UITextField]] = []
    
    let playerViewController = AVPlayerViewController()
    let player = AVPlayer()
    var observerStatus: NSKeyValueObservation?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        subsView.addSubview(scrollView)
        scrollView.addSubview(stackView)
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.leadingAnchor.constraint(equalTo: subsView.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: subsView.trailingAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: subsView.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: subsView.bottomAnchor).isActive = true
        
        stackView.axis = .vertical
        stackView.distribution  = .equalSpacing
        stackView.alignment = .center
        stackView.spacing   = 10
        stackView.backgroundColor = .cyan
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.leadingAnchor.constraint(equalTo: subsView.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: subsView.trailingAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        let subtitleDescView = UIStackView()
        stackView.addArrangedSubview(subtitleDescView)
        subtitleDescView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor).isActive = true
        subtitleDescView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor).isActive = true
        subtitleDescView.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        subtitleDescView.axis = .horizontal
        subtitleDescView.alignment = .center
        
        
        let labelStart: UILabel = {
            let label = UILabel()
            label.text = "Start"
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        subtitleDescView.addArrangedSubview(labelStart)
        labelStart.widthAnchor.constraint(equalToConstant: 55).isActive = true
        
        let labelEnd: UILabel = {
            let label = UILabel()
            label.text = "End"
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        subtitleDescView.addArrangedSubview(labelEnd)
        labelEnd.widthAnchor.constraint(equalToConstant: 55).isActive = true
        
        let labelSubtitle: UILabel = {
            let label = UILabel()
            label.text = "Subtitle"
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        subtitleDescView.addArrangedSubview(labelSubtitle)
        labelSubtitle.leadingAnchor.constraint(equalTo: labelEnd.trailingAnchor).isActive = true
        labelSubtitle.trailingAnchor.constraint(equalTo: subtitleDescView.trailingAnchor).isActive = true
        
        playerViewController.player = player
    }
    
    @IBAction func watch(sender: UIButton) {

        let cloudNameValue = cloudName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let videoPublicIdValue = videoPublicId.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if (cloudNameValue!.isEmpty || videoPublicIdValue!.isEmpty || textFields.count == 0){
            self.alert(title: "Missing data", message: "You must fill Cloud Name, Video Public ID and Subtitles fields")
            return
        }
        var subtitles = [[String:String]]()

        for textFieldsGroup in textFields {
            let startTiming = NSString(string: textFieldsGroup[0].text ?? "0").doubleValue
            let endTiming = NSString(string: textFieldsGroup[1].text ?? "0").doubleValue
            let text = String(textFieldsGroup[2].text ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
            if (startTiming == 0.0 || endTiming == 0.0 || text.isEmpty){
                continue
            }
            subtitles.append([
                "start-timing": String(format: "%.3f", startTiming),
                "end-timing": String(format: "%.3f", endTiming),
                "text": text
            ])
        }
        if (subtitles.count == 0){
            self.alert(title: "Missing data", message: "Invalid Subtitles fields")
        }
        
        let url = MediaSubs().addSubtitlesToVideo(cloudName: cloudNameValue!, videoPublicId: videoPublicIdValue!, subtitles: JSON(["Subtitles": subtitles]))
        
        let urlo = URL(string: url)!
        
        let asset = AVURLAsset(url: urlo)
        let playerItem = AVPlayerItem(asset: asset)
        
        observerStatus = playerItem.observe(\.status, changeHandler: { [weak self] (item, value) in
            print("status: ", item.status)
            
            if item.status == .failed {
                print("failed")
                print(value)
                self?.playerViewController.dismiss(animated: true, completion: {
                    self?.alert(title: "Error", message: "Wrong parameters")
                })
            }
        })
        player.replaceCurrentItem(with: playerItem)

        self.present(playerViewController, animated: true) {
            self.playerViewController.player!.play()
        }
    
    }
    
    func alert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func addSub(sender: UIButton) {
        
        let subtitleView = UIStackView()
        stackView.addArrangedSubview(subtitleView)
        subtitleView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor).isActive = true
        subtitleView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor).isActive = true
        subtitleView.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        subtitleView.axis = .horizontal
        subtitleView.distribution  = .equalSpacing
                
        let inputStart: UITextField = {
            let textField = UITextField()
            textField.keyboardType = .decimalPad
            textField.borderStyle = .roundedRect
            return textField
        }()
        subtitleView.addArrangedSubview(inputStart)
        inputStart.delegate = self
        inputStart.addDoneButtonToKeyboard(myAction: #selector(inputStart.resignFirstResponder))
        inputStart.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        let inputEnd: UITextField = {
            let textField = UITextField()
            textField.keyboardType = .decimalPad
            textField.borderStyle = .roundedRect
            return textField
        }()
        subtitleView.addArrangedSubview(inputEnd)
        inputEnd.delegate = self
        inputEnd.addDoneButtonToKeyboard(myAction: #selector(inputEnd.resignFirstResponder))
        inputEnd.widthAnchor.constraint(equalToConstant: 50).isActive = true
        inputEnd.leadingAnchor.constraint(equalTo: inputStart.trailingAnchor, constant: 5).isActive = true
        
        let inputSubtitle: UITextField = {
            let textField = UITextField()
            textField.borderStyle = .roundedRect
            textField.returnKeyType = .done
            textField.delegate = self
            return textField
        }()
        subtitleView.addArrangedSubview(inputSubtitle)
        inputSubtitle.leadingAnchor.constraint(equalTo: inputEnd.trailingAnchor, constant: 5).isActive = true
        inputSubtitle.trailingAnchor.constraint(equalTo: subtitleView.trailingAnchor).isActive = true
        
        textFields.append([inputStart, inputEnd, inputSubtitle])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
