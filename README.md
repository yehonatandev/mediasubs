# MediaSubs

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

This pod is private, and avaliable for integration manualy.

Clone/Download this repo, and run the Example project:

`wget 'https://bitbucket.org/yehonatandev/mediasubs/get/v0.1.tar.gz'`
`tar xzf "yehonatandev-mediasubs-*"`
`cd "yehonatandev-mediasubs-*/Example"`
`pod install`
`open MediaSubs.xcworkspace`

And run the project with iOS 13 on any iPhone/iPad device

## Author

yehonatandev, yehonatan.kadosh@gmail.com

## License

MediaSubs is available under the MIT license. See the LICENSE file for more info.
